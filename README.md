## Create the project and deply on AWS Lambda

1. Create a new project
```
cargo lambda new <PROJECT_NAME>
```

2. Add dependencies to cargo.html and implement main.rs

3. Test every Lambda function

```
cd convert_to_uppercase
cargo lambda watch
cargo lambda invoke --data-file input1.json
```

```
cd remove_pinctuations
cargo lambda watch
cargo lambda invoke --data-file input2.json
```

4. Deploy on AWS Lambda
```
cargo lambda build --release
cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
```

![Alt text](screenshot1.jpg)
![Alt text](screenshot2.jpg)

## Step Functions Workflow

1. Create a new State Machine in AWS Step Functions
2. Implement the workflow coordinating the specific execution process of lambda functions. Here is the definition of the state machine of mine:

```
{
  "Comment": "This state machine transforms input text to uppercase and removes punctuations.",
  "StartAt": "TextToUppercase",
  "States": {
    "TextToUppercase": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:975050303721:function:convert_to_uppercase",
      "Next": "RemovePunctuations"
    },
    "RemovePunctuations": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:us-east-1:975050303721:function:remove_punctuations",
      "End": true
    }
  }
}
```

![Alt text](screenshot3.jpg)

3. Execute the state machine with input to test if the workflow can work correctly.

![Alt text](screenshot4.jpg)

![Alt text](screenshot5.jpg)

## Data Processing Pipeline

In the initial stage of the pipeline, labeled "ConvertToUppercase," the first Lambda function processes the input text, identified as 'data,' transforming all characters to uppercase. Subsequently, the pipeline advances to the "RemovePunctuations" stage, where a second Lambda function is tasked with eliminating all punctuations from the input, now referred to as 'uppercase'. These operations establish a straightforward data processing sequence. An illustrative diagram accompanies this description, effectively capturing the process. It shows the text as it appears post-uppercase conversion but prior to punctuations removal—characterized by the absence of lowercase letters, though punctuation marks are still present.

## Demo

Demo Link: https://www.youtube.com/watch?v=efQz-zxk4cs